package com.books.book;

import com.books.book.api.controller.BookController;
import com.books.book.api.model.Book;
import com.books.book.service.BookService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static java.lang.Boolean.TRUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BookApplicationTests {

    @Mock
    private BookService bookService;
    @InjectMocks
    private BookController bookController;

    @Test
    void verifyBookSearch() {
        Book book1 = new Book("aa", "bb");
        when(bookService.getBook("aa")).thenReturn(Optional.of(book1));
        Book book = bookController.getBook("aa");
        assertEquals(book1, book);
    }

    @Test
    void verifyBookDelete() {
        when(bookService.deleteBook("aa")).thenReturn(TRUE);
        String deletedBook = bookController.deleteBook("aa");
        assertEquals("Book deleted: Title:aa", deletedBook);
    }

    @Test
    void verifyBookAddition() {
        String title = "cc";
        String author = "dd";
        String result = bookController.addBook(title, author);
        verify(bookService).addBook(title, author);
        assertEquals("Book added: Title:cc Author:dd", result);
    }

    @Test
    void verifyListAllBooks() {
        List<Book> books = List.of(new Book("aa", "bb"), new Book("cc", "dd"));
        when(bookService.listAllBooks()).thenReturn(books);
        List<Book> result = bookController.listAllBooks();
        assertEquals(books, result);
    }

    @Test
    void verifyUpdateAuthor() {
        String title = "aa";
        String newAuthor = "newAuthor";
        String result = bookController.updateAuthor(title, newAuthor);
        verify(bookService).updateAuthor(title, newAuthor);
        assertEquals("Book author updated: Title:aa Author:newAuthor", result);
    }
}
