package com.books.book.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Scanner;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ArtifactScanningService {

    @Value("${API_KEY}")
    private String apiKey;
    private static final String METADEFENDER_HASH_URL = "https://api.metadefender.com/v4/hash/";
    private static final String METADEFENDER_UPLOAD_URL = "https://api.metadefender.com/v4/file";
    private static final String ARTIFACT_DIR = "build/libs";

    public  void validKey() {
        if (apiKey == null || apiKey.isEmpty()) {
            System.err.println("Error: METASCAN_API_KEY environment variable not set.");
            System.exit(1);
        }

        File artifactDir = new File(ARTIFACT_DIR);
        if (!artifactDir.exists() || !artifactDir.isDirectory()) {
            System.err.println("Error: Artifact directory does not exist.");
            System.exit(1);
        }

        File[] artifacts = artifactDir.listFiles();
        if (artifacts == null || artifacts.length == 0) {
            System.err.println("Error: No artifacts found in directory.");
            System.exit(1);
        }

        for (File artifact : artifacts) {
            try {
                System.out.println("Scanning artifact: " + artifact.getName());
                String hash = calculateSHA256(artifact);

                JSONObject hashResult = checkHash(hash);
                if (hashResult != null && hashResult.has("found") && hashResult.getBoolean("found")) {
                    String scanResult = hashResult.getJSONObject("scan_results").getString("scan_all_result_a");
                    System.out.println("Cached result: " + scanResult);
                    if (!"No Threat Detected".equals(scanResult)) {
                        failBuild(artifact.getName(), scanResult);
                    }
                } else {
                    System.out.println("No cached result. Uploading artifact...");
                    String dataId = uploadFile(artifact);
                    JSONObject scanResult = pollForResults(dataId);
                    String scanStatus = scanResult.getJSONObject("scan_results").getString("scan_all_result_a");
                    System.out.println("Scan result: " + scanStatus);
                    if (!"No Threat Detected".equals(scanStatus)) {
                        failBuild(artifact.getName(), scanStatus);
                    }
                }
            } catch (Exception e) {
                System.err.println("Error scanning artifact: " + artifact.getName());
                e.printStackTrace();
                System.exit(1);
            }
        }

        System.out.println("All artifacts scanned successfully. No threats detected.");
        System.exit(0);
    }

    private  String calculateSHA256(File file) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        try (FileInputStream fis = new FileInputStream(file)) {
            byte[] byteBuffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = fis.read(byteBuffer)) != -1) {
                digest.update(byteBuffer, 0, bytesRead);
            }
        }
        byte[] hashBytes = digest.digest();
        StringBuilder hashHex = new StringBuilder();
        for (byte b : hashBytes) {
            hashHex.append(String.format("%02x", b));
        }
        return hashHex.toString();
    }

    private  JSONObject checkHash(String hash) throws IOException {
        URL url = new URL(METADEFENDER_HASH_URL + hash);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("apikey", apiKey);
        connection.setRequestMethod("GET");

        try (Scanner scanner = new Scanner(connection.getInputStream(), StandardCharsets.UTF_8)) {
            String response = scanner.useDelimiter("\\A").next();
            return new JSONObject(response);
        } catch (IOException | JSONException e) {
            System.out.println("No cached result found.");
            return null;
        }
    }

    private  String uploadFile(File file) throws IOException, JSONException {
        URL url = new URL(METADEFENDER_UPLOAD_URL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("apikey", apiKey);
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=---Boundary");

        try (FileInputStream fis = new FileInputStream(file)) {
            String boundary = "---Boundary";
            String contentDisposition = "--" + boundary + "\r\nContent-Disposition: form-data; name=\"file\"; filename=\"" + file.getName() + "\"\r\n\r\n";
            connection.getOutputStream().write(contentDisposition.getBytes(StandardCharsets.UTF_8));

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = fis.read(buffer)) != -1) {
                connection.getOutputStream().write(buffer, 0, bytesRead);
            }

            connection.getOutputStream().write(("\r\n--" + boundary + "--\r\n").getBytes(StandardCharsets.UTF_8));

            try (Scanner scanner = new Scanner(connection.getInputStream(), StandardCharsets.UTF_8)) {
                String response = scanner.useDelimiter("\\A").next();
                return new JSONObject(response).getString("data_id");
            }
        }
    }

    private  JSONObject pollForResults(String dataId) throws Exception {
        URL url = new URL(METADEFENDER_UPLOAD_URL + "/" + dataId);

        while (true) {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("apikey", apiKey);
            connection.setRequestMethod("GET");

            try (Scanner scanner = new Scanner(connection.getInputStream(), StandardCharsets.UTF_8)) {
                String response = scanner.useDelimiter("\\A").next();
                JSONObject jsonResponse = new JSONObject(response);

                if (jsonResponse.has("scan_results")) {
                    JSONObject scanResults = jsonResponse.getJSONObject("scan_results");
                    String scanStatus = scanResults.getString("scan_all_result_a");

                    // If scan is completed, return the result
                    if (!scanStatus.equals("In Progress") && !scanStatus.equals("Queued")) {
                        return jsonResponse;
                    } else {
                        System.out.println("Scan still in progress... waiting...");
                    }
                }
            } catch (IOException e) {
                System.out.println("Waiting for scan results...");
            }

            // Wait for 10 seconds before checking again
            Thread.sleep(10000);
        }
    }



    private  void failBuild(String artifact, String reason) {
        System.err.println("Build failed: " + artifact + " - " + reason);
        System.exit(1);
    }
}