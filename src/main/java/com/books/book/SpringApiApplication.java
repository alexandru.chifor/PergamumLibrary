package com.books.book;

import com.books.book.service.ArtifactScanningService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringApiApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SpringApiApplication.class, args);
        ArtifactScanningService service = context.getBean(ArtifactScanningService.class);
        service.validKey();
    }
}
